<!--[![Build Status](https://travis-ci.org/satnet-project/server.svg?branch=development_3k)](https://travis-ci.org/satnet-project/server)-->

[![Circle CI](https://circleci.com/gh/satnet-project/server.svg?style=shield)](https://circleci.com/gh/satnet-project/server)
[![Coverage Status](https://coveralls.io/repos/satnet-project/server/badge.svg?branch=development_3k)](https://coveralls.io/r/satnet-project/server?branch=development_3k)
[![Code Health](https://landscape.io/github/satnet-project/server/master/landscape.svg?style=flat)](https://landscape.io/github/satnet-project/server/master)

satnet-server
================

Repository with the source code for the server of the SATNet network.

To install the software for the server of the SATNet network, follow the instructions within the INSTALL file. The included installation script is a helper for the installation process and not yet a final completely automated solution. Any help on this issue will be much appreciated.

The wiki together with documentation of the project can be found at:

https://github.com/satnet-project/satnet-main/wiki
	
Online Demo (LEOP operations)
================

https://satnet.aero.calpoly.edu/leop_staff/test
